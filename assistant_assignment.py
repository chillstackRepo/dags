import airflow
from airflow.models import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
import datetime
from datetime import timedelta
from functools import partial
import os

def call_on_failure(args,context):
    print(context)
    os.system("sh /home/ubuntu/airflow/dags/alert.sh "+args)

args = {
    'owner': 'Ectosense',
    'start_date': datetime.datetime(2020,10,21),
    'on_failure_callback':partial(call_on_failure,"assistant_assignment Cron")
}

dag = DAG(
    dag_id='assistant_assignment',
    default_args=args,
    schedule_interval='*/1 * * * *',
    dagrun_timeout=timedelta(minutes=30),
    catchup=False
)

assistant_assignment = BashOperator(
    task_id='assistant_assignment',
    bash_command='node  /home/ubuntu/ectosense-backend/controllers/patientController/sqsReciever.js',
    dag=dag,
)


assistant_assignment 