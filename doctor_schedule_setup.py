import airflow
from airflow.models import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
import datetime
from datetime import timedelta
from functools import partial
import os

def call_on_failure(args,context):
    print(context)
    os.system("sh /home/ubuntu/airflow/dags/alert.sh "+args)

args = {
    'owner': 'Ectosense',
    'start_date': datetime.datetime(2020,10,20),
    'on_failure_callback':partial(call_on_failure,"doctor_schedule_setup Cron")
}

dag = DAG(
    dag_id='doctor_schedule_setup',
    default_args=args,
    schedule_interval='5 0 * * *',
    dagrun_timeout=timedelta(minutes=30),
    catchup=False
)

doctor_schedule_setup = BashOperator(
    task_id='doctor_schedule_setup',
    bash_command='node /home/ubuntu/ectosense-backend/utills/doctorTimeSlotInsertion.js',
        env={'message': '{{ dag_run.conf["message"] if dag_run else "" }}'},

    dag=dag,
)


doctor_schedule_setup 